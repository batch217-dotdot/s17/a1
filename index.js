/*
1. Create a function which is able to prompt the user to provide their fullname, age, and location.
	-use prompt() and store the returned value into a function scoped variable within the function.
	-display the user's inputs in messages in the console.
	-invoke the function to display your information in the console.
	-follow the naming conventions for functions.
*/
//first function here:
function getUserInfo() {
	let fullName = prompt("Enter your full name.");
	let age = prompt("Enter your age.");
	let location = prompt("Enter your location.");

	function displayUserInfo() {
		console.log("Hello, " + fullName);
		console.log("You are " + age + " years old.");
		console.log("You live in " + location);
	}
	alert("Thank you for your input.");
	displayUserInfo();
}
getUserInfo();



/*
2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
	-invoke the function to display your information in the console.
	-follow the naming conventions for functions.

*/
//second function here:
function displayFavBands() {
	let favBands = ["The Beatles", "Metallica", "The Eagles", "L'arc~en~Ciel", "Eraserheads"];
	console.log("1. " + favBands[0]);
	console.log("2. " + favBands[1]);
	console.log("3. " + favBands[2]);
	console.log("4. " + favBands[3]);
	console.log("5. " + favBands[4]);
}
displayFavBands();
/*
3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
	-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
	-invoke the function to display your information in the console.
	-follow the naming conventions for functions.

*/
//third function here:
function displayFavMovies() {
	let favMovies = [
		{
			"movieName" : "The Godfather",
			"rating" : "97%"
		},
		{
			"movieName" : "The Godfather, Part II",
			"rating" : "96%"
		},
		{
			"movieName" : "Shawshank Redemption",
			"rating" : "91%"
		},
		{
			"movieName" : "To Kill A Mockingbird",
			"rating" : "93%"
		},
		{
			"movieName" : "Psycho",
			"rating" : "96%"
		}
	];
	console.log("1. " + favMovies[0].movieName);
	console.log("Rotten Tomatoes Rating: " + favMovies[0].rating);
	console.log("2. " + favMovies[1].movieName);
	console.log("Rotten Tomatoes Rating: " + favMovies[1].rating);
	console.log("3. " + favMovies[2].movieName);
	console.log("Rotten Tomatoes Rating: " + favMovies[2].rating);
	console.log("4. " + favMovies[3].movieName);
	console.log("Rotten Tomatoes Rating: " + favMovies[3].rating);
	console.log("5. " + favMovies[4].movieName);
	console.log("Rotten Tomatoes Rating: " + favMovies[4].rating);
}
displayFavMovies();
/*
4. Debugging Practice - Debug the following codes and functions to avoid errors.
	-check the variable names
	-check the variable scope
	-check function invocation/declaration
	-comment out unusable codes.
*/

let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();
// console.log(friend1);
// console.log(friend2);